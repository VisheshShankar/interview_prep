Things to focus on:
- Collaboration
  - How did you work with teams across boundaries
  - The most vastly different the better
  - Show ways how I impact teams that I don't typically impact
  - Show ability pick up on diverse perspective
- How did you handle being wrong?
  - Able to take in new perspectives!
- Make sure to talk about how you broke out a problem (what were the options to solve and outcomes to figure)
- Mention out how you were able to "scale" through other people (either passing off, mentoring some else or enabling a wider audience)
- Outcome & Impact. Why what **I** did was important/relevant

Make sure I:
- Ask specific questions and work culture of that team
  - I need to get across that "I am looking for this specific type of team"
- State your assumptions if you are clarifying the questions.