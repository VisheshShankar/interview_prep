# interview_prep

Random Scribblings to help prep for interviews

# Source Material
    Based on the information/questions found here
        1. https://leetcode.com/discuss/interview-question/281149/Facebook-or-Phone-or-Data-engineer
            Question 3 stumped me for a bit... here are my notes on what I had to play around with:

``` python
    #Tried this to start
        def flattenList(someArray):
            return [newlist for array in someArray]    

        ListOfCombos = numpy.array_split(ListofMemberCombos, len(ListofMemberCombos))
        ListOfCombos = flattenList(ListofMemberCombos)
    # Tried a bunch of array splits... got fed up and stealing this
    # https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-a-list-of-lists
    # This works but doesn't give me what I want
        ListOfCombos = [item for sublist in ListofMemberCombos for item in ListofMemberCombos]
    # This is eaxctly what I want - will clean up in my code.
        ListOfCombos = [item for item in ListofMemberCombos]
```
        2. https://leetcode.com/discuss/interview-question/586048/Facebook-or-Phone-or-Mismatched-sentences-and-Fill-an-Array
        3. https://leetcode.com/discuss/interview-question/807990/Facebook-SQL-questions-DE
        4. https://leetcode.com/discuss/interview-question/896522/Facebook-Data-Engineering-Interview-Onsite
        5. https://leetcode.com/discuss/interview-question/1084674/Facebook-Data-Engineer-Londonor-Onsite-Scheduled
        6. https://www.glassdoor.com/Interview/Meta-Data-Engineer-Interview-Questions-EI_IE40772.0,4_KO5,18.htm#InterviewReview_51256507
        7. https://www.glassdoor.com/Interview/Meta-Data-Engineer-Interview-Questions-EI_IE40772.0,4_KO5,18.htm#InterviewReview_43703957
        8. https://www.glassdoor.com/Interview/Meta-Data-Engineer-Interview-Questions-EI_IE40772.0,4_KO5,18.htm#InterviewReview_54387198
        9. https://www.youtube.com/watch?v=3AHzWOWRWvQ 

# Questions from Meta
    ```
    Write a function that returns the elements on odd positions (0 based) in a list
    def solution(input):    
    #code goes here 
    return output
    
    assert solution([0,1,2,3,4,5]) == [1,3,5]
    assert solution([1,-1,2,-2]) == [-1,-2]

    Write a function that returns the cumulative sum of elements in a list
    def solution(input):    
    # Code goes here     
    return output
    
    assert solution([1,1,1]) == [1,2,3]
    assert solution([1,-1,3]) == [1,0,3]
    
    Write a function that takes a number and returns a list of its digits
    def solution(input):    
    # Code goes here    
    return output
    
    assert solution(123) == [1,2,3]
    assert solution(400) == [4,0,0]
    
    From: http://codingbat.com/prob/p126968Return the "centered" average of an array of ints, which we'll say is the mean average of the values, except ignoring the largest and smallest values in the array. If there are multiple copies of the smallest value, ignore just one copy, and likewise for the largest value. Use int division to produce the final average. You may assume that the array is length 3 or more.
    
    def solution(input):
    # Code goes here    return output 
    
    assert solution([1, 2, 3, 4, 100]) == 3
    assert solution([1, 1, 5, 5, 10, 8, 7]) == 5
    assert solution([-10, -4, -2, -4, -2, 0]) == -3


    ```
# Other Materials
    These were also helpful to review:
        - https://igotanoffer.com/blogs/tech/facebook-data-engineer-interview
        - https://cdn.shopify.com/s/files/1/0417/7869/files/FB_onsite_interview_Data_Engineer.pdf?v=1622192469
            - https://www.infoq.com/articles/netflix-migrating-stream-processing/
            - https://pgexercises.com/questions/aggregates/
        - https://www.interviewkickstart.com/interview-questions/facebook-data-engineer-interview-questions
        - https://prepfully.com/interview-guides/facebook-data-engineer

