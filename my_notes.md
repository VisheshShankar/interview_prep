When doing any product sense, you can always break it down by:
- Outliers that need to be tracked
- Geographical breakdown (region, marketplace, weather, etc)
- Time (happening suddenly/progressively, weekly/monthly trends, compare variance over time periods)
- Segment by "target audience" (typically customer, renter, driver, home owner)



Company Key Metrics:
- AirBnB:
  - Number of Hosts// Number of New Hosts
    - Ratio of New to Retained
  - Number of Guests (users)// Number of New Guests
    - Ratio of New to Retained
  - Number of Gross Nights booked (Total number of days booked by users)
  - Revenue garnered for Hosts// AirBnB
  - Segments:
    - Categories of Lengths of Trip (Short/Medium/Long)
    - Categories by Geography
    - Categories by new Feature types?
    