key things reviewed during tech interview

- Product sense
- data modeling
- SQL
- Python
- ETL

Go to Meta's news room and get in on their products

about.fb.com/news --> 

tech 1 & tech 2 are ETL focused (hour long, very similar. Questions will be focused on two different Meta products. CAn be real or make believe)
- Key Skills
	- Product sense
	- SQL
	- Python
	- ETL
- Both start of with a particular problem, similar to a case study
- Will start off with product sense questions to determine how we focus on the product (10-15 minutes)
  - focus on product interpretation. Active Discussion with interviewer
  - I will need to come with ways to describe the model (dimensions
    - Looking at DAU and MAU --> massive spike in DAU. What do you think happened?
	  - example: surges due to covid, meta pushing new product, people having more time on their hands, weekend vs weekday, update on blah blah
  - Focus on Depth, Extent and relevancy
    - Depth: amount of ideas you can come up with (come up with as many solution as possible, 3+)
	  - Extent: What are these ideas telling us and why are they important
	  - Relevancy: Should be relevant to the particular product you are discussing
  - Biggest mistake they they make:
    - Candidates only deep dive into one idea... and keep going back into that
- ETL problems (will see 3 or 4)

- PRoduct sense section of tech 2 would be most likely spot to talk about vizualiation
  - Can be pretty/ugly... but want to focus on the type of viz for the vizualation
    - Don't do a pie chart for WoW
- Suggestion:
  - Review the key operating metrics that determine success and how they get to those metrics
  
- AFter product sense, interviewer will provide the schema or data related to the product. (15 minutes SQL/30 minutes Python)
  - Interviewer will ask further questions to use the schema provided and use SQL/Python to answer questions
  - will develop process for batch data and real time streaming data
    - batch: data delivered in daily batches (often more SQL heavy)
	- streaming: real time events/continuously (often more Python heavy) (regular loop you will need to provide)
  - Will ETL from source to target
    - Source: Logging tables (captures data from user actions)
	- Target: Aggregate tables or data objects that satisfy specific use case
  - SQL: moderate/advanced. Single Table Scans on!
  - Python: basic/basic-intermediate: keep answers simple solve for majority, not edge case
  

tech 3 are data modeling
- KEy skills
	- Product sense
	- data modeling
	- SQL
- GRound up creating data model --> probing what is the data that is needed
- Will start off with Product Sense -  will be more ambigious
  - I should be driving the discussion in order to define relevant metrics/dimensions that are NEEDED
- Interviewer will provide an external product for me to develop
  - streaming: Neflix/Hulu
  - Ride SHARE
  - vacation rental
  - Most likely have an app/tech surrounding them
  --> Notice how the metrics needed for this are gonna be different
- Kimball's data modelling' --> good to review/extra insight
- Need to create a data model (whiteboard, pen/paper)
  - Need to be able to design fact/dimenssion tables
  - Star/snowflake schema
- Focus ON:
  - scalability/maintainability
  - appropiate level of normalization/denormalization & trade offs/implications on performance
- Interviewer will ask questions based on the model that I have created to be solved with SQL
  - Up to all 8
- Do not think of data model as a completed product

- Key things to remember:
  - ETL sections: 
    - based on facebook products
    - product sense guided by interviewer
	- raw information provided (data/inputs/etc)
	- SQL/Python
  - Data modeling:
    - blatant copy of external company
	- product sense driven by me
	- raw information not provided
	- SQL only
  - Get stuck, make sure to speak thoughts out loud

Ownership interview
- Understanding of my role and how I define it
  - Understand the difference between DS and DE
  - Differnce betwen DE and SDE
  - Why Data Engineering for me
  - Talk DE from end to end and how it has it impacts
- Be detailed and concise
- Leobrian's description
  - End to End role with the product team, working with PMs and DSs. DE are at front seat of the table, directing with the products, working with raw data and creating reports for decision makers
- Experience with data modeling and volumes of data I have worked with
- Current Tools/tech that we are using
- How does my current role match with the DE role
- Have two DE focused projects in backpocket
  - Talk in great detail and end to end.
  - Talk about problems that occured
  - Cross functional relationships
  - MY key responsibilities
  - Roadblocks
  - Outcome & Impact. Why what **I** did was important/relevant
- Share time where I took initiative to start
- Amazing how we used data to influence the company or the org
- How has other people use data influenced you to change your mind
- Talk about how I prioritize/organize my work - how do I stay balanced? How do i determine what is important?
- 30/60/90 roadmap for new role - What are my goals for each of those benchmarks
  - Clearly structured output goal after 90
  - Where I see myself at end of each month
  - Need to have a feedback loop
  - Want to see that you have a plan
  - break it down by month and 3-4 bullet points for each
  - First month:
    - Learn systems/ key tables
	- Bootcamp
  - second month:
	- set up 1:! with managers + team members
	- Review in flight projects/completed projects
  - last month:
    - start working with x-functional teams
	- start working on small project
  - end goal: Plan on completing my first code review
- Tell me a time where you had to disagree with a team mate/ how have you pushed back to business



Meeting with Seth:
- Talk about how you have identified a signal (Attach Problem, FLX project)
- Make sure to talk about how you broke out a problem (what were the options to solve and outcomes to figure)
- Mention out how you were able to "scale" through other people (either passing off, mentoring some else or enabling a wider audience)
- What do you do about late posting data? Incomplete Data? Malformed? 
  - How are you checking for this?
  - What day do you post it to? What are the criteria that determine that?
  - What are the trade offs? What is the tolerance for signal to noise ratio?
  - Do you optimize when it comes in (add indices/partitions)
  - What are my corner/edge cases? Ask about contingency
- State your assumptions if you are clarifying the questions.
- Who is the customer that will use this? Who is their customer?
  - Is this a Big C customer or Little C Customer
- What is your 30/60/90 plan?
  - Want to build relationships with my colleagues --> 1:1 with as many of them as possible
  - Build my brand and let peopl know what I am about
  - Understand customers and their needs... if possible understand their customers.
- How I structure my data engineering to cross different database/schemas and minimize movement of data

Meeting with Abebe:
- Across the board, the processes are the same.
- Key thing: Collaboration
  - How did you work with teams across boundaries
  - The most vastly different the better
  - Show ways how I impact teams that I don't typically impact
  - Show ability pick up on diverse perspective
- How did you handle being wrong?
  - Able to take in new perspectives!
- Be able to explain concise information on data on what I work
  - Meta loves the metrics side of things
- Ask specific questions and work culture of that team
  - I need to get across that "I am looking for this specific type of team"
- If candidate was set in their way with their strategy
  - Interviewer will sway you to get in the right space
  - LISTEN to their hints