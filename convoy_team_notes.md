###Chat with Tim

Company Goals and Insights?
Core initiative to reduce environmental impact --> how can we reduce that

How do you see the company growing over next 3-5 years? What initiatives are currently in the works to support that?
Rebalancing of trailers
bundling
reducing empty miles
having more efficient routes


What does the DS team at Convoy do? Why is there a big push for BIEs? How do you see me coming in and making an impact?
- Making huge investment in DS and BIE roles (all spectrums)
- BIE role as right in the middle of central and strategy
  - Technical function to enable that we can make data informed decision --> measuring impact and we are alerted
  - Strategy piece - because they are experts in the metrics, they end up in the middle of the conversation on which features/what to go after/what pivots do we need on/what do we need to double down on?


SStack:
- Snowflake
- Airflow/DBT
- Metabase 


When pandemic happened, they started to focus unit economics, to tighten the nuts and bolts.
- This was ahead of time. "Took a year off" during Pandemic. Growth started to pick back up in 2021.
- Starting to build a positive margin. 
- Ripped off some bandaids

What would be my two big things:
- Measuring how our products are doing?
  - How many times are our products being used in the marketplace
  - How many times are are they successful/not
  - What geography are they being used?
- "Here is where our data is broken/instrumentation that is needed" --> hand this long list to DE team to get the data
 

###Chat with Dorothy

What made you choose Convoy?
- At Amazon for a long time; Built Quicksight
  - Not in supply chain or transportation ever
- Wanted to join a late stage startup
  - joining another big company is just solving another set of problems
- Wanted to go to a new company to where there was new problems
- WAnted to join company that could scale and would pre-ipo
- Need to know who the leadership were, wanted to admire the leadership
- Mission aspect: Work on something that is has a POSITIVE IMPACT

When she joined - was CTO
- Joined with mindset that it is a startup and will do whatever it takes
- Opportunity to consolidate DS, PRoduct and Engineering can work under a SLT
  - Changed faster than she has expected
- This speeds up the ability to to speed up how fast we can build up the products
  - More ownership across the 
- School on how to be an entreprenuer
- The problem space is more complex than you expect
  - More dimensions than expected
  - More business than expected
    - All types of verticals

Prove, Prove or Scale
- Moonshot week = company shuts down to innovate (no just hackathon with engineers)
- We need to check in at certain milestones if it has "product market fit" and then we think about how it scale
- All types of budgeting going into it - how much do we spend on critical, low risk, high risk.

Company Goals and Insights?
- How do we properly cut the data
- Density at network/lane level?
- Cohorts of Carriers
- Great intersection between data and data science

How do you see the company growing over next 3-5 years? What initiatives are currently in the works to support that?

What excites you most about this last year to "get foundation right"?

What do you see as the biggest challenges to growth?