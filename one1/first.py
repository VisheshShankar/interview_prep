# find the average length of word in sentence
import sys


def avgwordlength(sentence):
    allwords = sentence.split()
    # This is how I would do it in any language
    # totallength = 0
    # for word in allwords:
    #     totallength = totallength + len(word)
    # This is how it is "supposed" to be done in Python.
    numberofwords = len(allwords)
    totallength = sum(len(word) for word in allwords)

    print(totallength/numberofwords)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        usersentence = sys.argv[1]
    else:
        usersentence = "THIS IS THE DEFAULT SENTENCE"
    avgwordlength(usersentence)