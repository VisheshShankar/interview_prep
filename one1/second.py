import sys

# Validate the ip address

# What do you need to do to validate an IP Address?
# IPv4 addresses are canonically represented in dot-decimal notation,
# which consists of four decimal numbers, each ranging from 0 to 255,
# separated by dots, e.g., 172.16.254.1.
# The generalized form of an IPv4 address is (0-255).(0-255).(0-255).(0-255).


def validateIP(IPAddress):
    AddressPieces = IPAddress.split('.')
    # If it is not a four part address, immediately it is wrong.
    if len(AddressPieces) != 4:
        return False
    for EachPiece in AddressPieces:
        # Check to see if it is a number.
        # When it isn't, retunr false and exit function
        if not EachPiece.isdigit():
            return False
        # Otherwise, validate it to be with the 0-255 range
        else:
            if int(EachPiece) > 255 or int(EachPiece) < 0:
                return False
    return True


if __name__ == "__main__":
    if len(sys.argv) > 1:
        userIPAddress = sys.argv[1]
    else:
        userIPAddress = "255.123.111.10"
    # This is how I would typically do it
    # isValid = validateIP(userIPAddress)
    # print(isValid)
    # This is pythonic.
    print(validateIP(userIPAddress))