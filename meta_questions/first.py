def solution(input_list):
    output = []
    for x in input_list:
        if input_list.index(x) % 2 == 1:
            output.append(x)

    return output

print(solution([0,1,2,3,4,5]) == [1,3,5])
print(solution([1,-1,2,-2]) == [-1,-2])

# assert solution([0,1,2,3,4,5]) == [1,3,5]