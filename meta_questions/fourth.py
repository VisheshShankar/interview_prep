def solution(input_list):
    output = []
    temp_list = []
    input_list.sort()
    # print(input_list)
    temp_list = input_list[1:-1]
    # print(temp_list)
    output = sum(temp_list)//int(len(temp_list))
    return output 

# print(solution([1, 1, 5, 5, 10, 8, 7]))

print(solution([1, 2, 3, 4, 100]) == 3)
print(solution([1, 1, 5, 5, 10, 8, 7]) == 5)
print(solution([-10, -4, -2, -4, -2, 0]) == -3)


# assert solution([1, 2, 3, 4, 100]) == 3
# assert solution([1, 1, 5, 5, 10, 8, 7]) == 5
# assert solution([-10, -4, -2, -4, -2, 0]) == -3