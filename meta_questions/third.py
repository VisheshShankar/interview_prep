def solution(input_integer):
    output = []
    input_string = str(input_integer)
    for digit in input_string:
        output.append(int(digit))    
    return output

print(solution(123) == [1,2,3])
print(solution(400) == [4,0,0])

# assert solution(123) == [1,2,3]
# assert solution(400) == [4,0,0]