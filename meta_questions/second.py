def solution(input_list):    
    output = []
    runninglist = []
    for x in input_list:
        runninglist.append(x)
        output.append(sum(runninglist))

    return output


print(solution([1,1,1]) == [1,2,3])
print(solution([1,-1,3]) == [1,0,3])

# assert solution([1,1,1]) == [1,2,3]
# assert solution([1,-1,3]) == [1,0,3]