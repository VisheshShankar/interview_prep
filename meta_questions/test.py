def question_one():
    string = 'mississippi'
    char = 's'

    return string.count(char)


def question_two(og_list):
    new_list = []
    prev = None
    for num in og_list:
        if num is not None:
            new_list.append(num)
            prev = num
        else:
            new_list.append(prev)

    return new_list


print(question_one())
print(question_two([1, None, 1, 2, None]))
print(question_two([1, None, 2, 3, None, None, 5, None]))
